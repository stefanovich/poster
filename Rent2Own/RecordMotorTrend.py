from PosterRecord import *
import PosterHelper

import PosterGrabber


class RecordMotorTrend (Record):
    __URL_LIST__ = 'https://www.motortrend.com/auto-news/'

    def __init__(self, url):
        super().__init__(url)

    @classmethod
    def get_records(cls):
        soup = PosterGrabber.get_html(cls.__URL_LIST__)
        articles = soup.select('.block-post')

        from collections import OrderedDict
        records = OrderedDict()

        for article in articles:
            article_url = RecordMotorTrend.get_url(article)
            records[article_url] = RecordMotorTrend(article_url)

        return records

    @staticmethod
    def is_sponsored_content(node):
        return len(node.find_elements_by_class_name('prx-promoted'))

    @staticmethod
    def get_url(article):
        return article.select_one('.block-post-title')['href']

    @staticmethod
    def get_title(article):
        tag = article.select_one('div.entry-header h1.entry-title.-title')
        return tag.get_text().strip()

    @staticmethod
    def get_summary(article):
        tag = article.select_one(
            'div.entry-header span.entry-title.-subtitle')
        return tag.get_text().strip()

    @staticmethod
    def get_images(article):
        title_image_tag = article.select_one(
            'div.featured-image div.image')

        article_image_tags = article.select(
            'figure.imagecontainer img')

        images = [
            PosterHelper.crop_url(
                title_image_tag['data-src'])]

        for image_tag in article_image_tags:
            image_url = PosterHelper.crop_url(
                image_tag['data-src'])
            if image_url not in images:
                images.append(image_url)

        return images

    def get_twitter_info(self):
        return self.__get_info()

    def get_facebook_info(self):
        return self.__get_info()

    def __get_info(self):
        soup = PosterGrabber.get_html(self.url)
        images = self.get_images(soup)
        text = self.build_summary([
            RecordMotorTrend.get_title(soup),
            RecordMotorTrend.get_summary(soup)
        ])

        return {
            'link':    self.url,
            'images':  images,
            'text':    text,
        }
