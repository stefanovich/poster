from PosterRecord import *
import PosterGrabber


class RecordCarAndDriver (Record):
    __URL_BASE__ = 'https://caranddriver.com'
    __URL_LIST__ = 'https://blog.caranddriver.com'
    __URL_IMAGE_BASE__ = 'https://hips.hearstapps.com/'

    def __init__(self, url):
        super().__init__(url)

    @classmethod
    def get_records(cls):
        from collections import OrderedDict
        records = OrderedDict()

        soup = PosterGrabber.get_html(cls.__URL_LIST__)
        for article in soup('cd-article-summary'):
            url_raw = RecordCarAndDriver.get_url(article)
            url_full = RecordCarAndDriver.__URL_BASE__ + url_raw
            records[url_raw] = RecordCarAndDriver(url_full)

        return records

    @staticmethod
    def get_title(wrapper):
        return wrapper.find('title').text

    @staticmethod
    def get_summary(wrapper):
        meta = wrapper.find('meta', {'name': 'description'})
        return meta['content']

    @staticmethod
    def get_url(wrapper):
        tag = wrapper.find('a')
        return tag['href'] if tag else None

    @staticmethod
    def get_images(wrapper):
        title_image_sources = [
            'div.hover-filter a.gtm-image-link img',
            'picture.gtm-hips-picture img',
            'span.inline-image img',
        ]

        images = list()
        for source in title_image_sources:
            tags = wrapper.select(source)
            for tag in tags:
                src_preload = tag.get('src')
                src_on_show = tag.get('data-src')

                images.append(
                    PosterHelper.crop_url(
                        src_preload or RecordCarAndDriver.__URL_IMAGE_BASE__ + src_on_show))

        return images

    def get_twitter_info(self):
        return self.__get_info()

    def get_facebook_info(self):
        return self.__get_info()

    def __get_info(self):
        print("Getting record info")

        soup = PosterGrabber.get_html(self.url)
        article = soup.select_one('div.article-col-width')

        images = self.get_images(article)
        text = RecordCarAndDriver.build_summary([
            RecordCarAndDriver.get_title(soup),
            RecordCarAndDriver.get_summary(soup),
        ])

        return {
            'link':    self.url,
            'images':  images,
            'text':    text,
        }
