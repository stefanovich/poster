import twitter
import facebook

# ToDo make Master as singleton
__master_set = dict()

ATTEMPTS_DEFAULT = 3
ATTEMPTS_SHORTENER = ATTEMPTS_DEFAULT
ATTEMPTS_FACEBOOK = ATTEMPTS_DEFAULT
ATTEMPTS_TWITTER = ATTEMPTS_DEFAULT


def masters():
    return __master_set


def register_master(alias, master):
    __master_set[alias] = master


def debug_twitter_token():
    return {
        'consumer_key':    'NVVcefxs0QYyapdLKQZXdZ8Hd',
        'consumer_secret': '5Vo1KQ6qTILPLEV69pii0YQywyO5mq9VhpvKtO7t3dcUNUKVvi',
        'access_key':      '168206572-HbA2vqCWANnNDMS0tJGJDYSaSVQF1CumfYYZCrXF',
        'access_secret':   '94ZVDhq85czbevkdfGvoA9YifI3PoMf0040pqOQRGjP7e',
    }


def debug_facebook_token():
    return get_facebook_token('373574129759628')


def get_facebook_token(page_id):
    # http://nodotcom.org/python-facebook-tutorial.html

    access_token = None
    user_access_token = \
        'EAAcD5OvwFogBAOGQKBzJZBGa33ScDoxazvPJ2bukt1FX' \
        'z2EPo3fNA3PHx1B1ZBzcHQBlQzXyHpY6gcZArVlZCOGXH' \
        'fijcGxexhcUVZASkIpntpYiP9YzU0D24zSKg7nw1wi5jZ' \
        'CrGYYeK1WODF8ZCfwILtHAwtdRuhcT6uZC5vf3ZBwZDZD'

    try:
        graph = facebook.GraphAPI(user_access_token)
        accounts = graph.get_object('me/accounts')

        for page in accounts.get('data'):
            if page['id'] == page_id:
                access_token = page['access_token']

    except facebook.GraphAPIError as error:
        print(error)

    return access_token


def get_short_link(url, shortener='Google', token=None):
    print('Generating short link for \'%s\'' % url)
    from pyshorteners import Shortener, exceptions

    if not token:
        if shortener == 'Google':
            token = 'AIzaSyASRIF0WJPftrc-KYtGftWYjUJ8yokqn_c'

        elif shortener == 'Bitly':
            token = '7728f16fbbc67269fca96de5887ec868e9040ff8'

    link = None
    for attempt in range(ATTEMPTS_SHORTENER):
        print('Attempt #%d' % (attempt + 1))
        try:
            link = Shortener(shortener, api_key=token, bitly_token=token).short(url)
            print('Link generated: %s' % link)
            break

        except (TypeError, ValueError) as error:
            print('Wrong type or value exception: %s' % error)

        except (exceptions.ExpandingErrorException,
                exceptions.ShorteningErrorException,
                exceptions.UnknownShortenerException) as error:
            print('Shortener exception: %s' % error)

    return link


def __twitter_api(token):
    return twitter.Api(
        tweet_mode='extended',
        consumer_key=token.get('consumer_key'),
        consumer_secret=token.get('consumer_secret'),
        access_token_key=token.get('access_key'),
        access_token_secret=token.get('access_secret'))


def post_twitter_status(record, token):
    success = False
    status_length = 265
    status_link_pattern = 'https://t.co/1234567890'
    info = record.get_twitter_info()

    status_media = info['images'][:4]
    status_link = info['link']
    status_text = crop_text(
        info['text'], status_length - len(status_link_pattern),
        suffix=' ') + status_link

    print('Posting twitter status:')
    print('\t' 'status: %s' % status_text)
    print('\t' 'media: %s' % status_media)

    for attempt in range(ATTEMPTS_TWITTER):
        try:
            print('Attempt #%d:' % (attempt + 1))
            __twitter_api(token).\
                PostUpdate(
                    status_text,
                    media=status_media,
                    verify_status_length=False)

            print('Status posted successfully')
            success = True
            break

        except twitter.TwitterError as error:
            print('Twitter errors occur:')
            print('\t%s' % error.message)

    return success


def post_facebook_record(record, token):
    success = False
    message_length = 256
    api = facebook.GraphAPI(token)
    info = record.get_facebook_info()

    link = info['link']

    images = info['images']
    image = images[0]\
        if images and len(images) else None

    message = crop_text(
        info['text'], message_length,
        suffix=' ' + info.get('link'))

    print('Posting facebook message:')
    print('\t%s' % message)
    for attempt in range(ATTEMPTS_FACEBOOK):
        try:
            print('Attempt #%d:' % (attempt + 1))
            api.put_object(
                parent_object="me",
                connection_name="feed",
                message=message,
                picture=image,
                link=link)

            print('Message posted successfully')
            success = True
            break

        except AssertionError as error:
            print('Facebook error occur:')
            print('\t%s' % error)

    return success


def save_file(content, path, filename=None, binary=False):
    import os
    os.makedirs(path, exist_ok=True)

    if filename and len(filename):
        path += os.sep + filename

    mode = 'wb' if binary else 'w'
    file = open(path, mode)
    file.write(content)
    file.close()

    return path


def crop_url(url):
    crop_at = url.find('?')
    return url[:crop_at] if crop_at > 0 else url


def normalize_image(path):
    print('Normalizing image \'%s\'' % path)
    from PIL import Image

    image = Image.open(path)
    image = crop_image(image)

    print('Saving image \'%s\'' % path)
    image.save(path)


def crop_image(image, color='white', delta=32):
    from PIL import ImageColor
    import random

    pix = image.load()
    crop_factor = sum(ImageColor.getrgb(color)) - delta
    crop_xl, crop_yt, crop_xr, crop_yb = 0, 0, 0, 0

    for crop_xl in range(image.width):
        if sum(pix[crop_xl, image.height * random.random()][:3]) < crop_factor:
            break

    for crop_xr in reversed(range(image.width)):
        if sum(pix[crop_xr, image.height * random.random()][:3]) < crop_factor:
            break

    for crop_yt in range(image.height):
        if sum(pix[image.width * random.random(), crop_yt][:3]) < crop_factor:
            break

    for crop_yb in reversed(range(image.height)):
        if sum(pix[image.width * random.random(), crop_yb][:3]) < crop_factor:
            break

    return image.crop((crop_xl, crop_yt, crop_xr, crop_yb))


def crop_text(text, max_length, prefix=str(), suffix=str()):
    max_length -= len(prefix) + len(suffix)
    if len(text) > max_length:
        text = text[:max_length - 2].strip('"\'., ') + '..'

    text += suffix
    return prefix + text
