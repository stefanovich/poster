

def get_html(url, parser='html.parser', encoding='UTF-8', **kwargs):
    content = get_text(url, encoding, **kwargs)

    from bs4 import BeautifulSoup
    return BeautifulSoup(content, parser)


def get_text(url, encoding=None, **kwargs):
    resource = get_resource(url, **kwargs)
    return resource.decode(encoding) \
        if encoding else resource.decode()


def get_resource(url, **kwargs):
    return __get_hyper(url, **kwargs) \
        if len(kwargs.keys()) \
        else __get_requests(url)


def __get_requests(url):
    print('\t' "getting resource via 'requests': {url}"
          .format(url=url))

    import requests
    response = requests.get(url)
    return response.content


def __get_hyper(url, **kwargs):
    import hyper

    print('\t' "getting resource via 'hyper': {url} ({kwargs})"
          .format(url=url, kwargs=kwargs))

    port = kwargs.get('port')
    method = kwargs.get('method')
    headers = kwargs.get('headers')
    data = kwargs.get('data')

    split_token = '://'
    if url.count(split_token):
        net, url = url.split(split_token)
        port = port or \
            443 if net is 'https' else \
            80 if net is 'http' else None

    host, path = url.split('/', 1)

    connection = hyper.HTTP20Connection(host, port=port)
    stream_id = connection.request(url=path, method=method,
                                   headers=headers, body=data)

    response = connection.get_response(stream_id)
    return response.read()
