from PosterRecord import *

import PosterGrabber
from collections import OrderedDict


class RecordMortgageReports (Record):
    __URL_BASE__ = 'https://themortgagereports.com'
    __URL_REAL_ESTATE__ = __URL_BASE__ + '/real-estate-news'
    __URL_MORTGAGE__ = __URL_BASE__ + '/mortgage-news'
    __URL_ECONOMIC__ = __URL_BASE__ + '/economic-news'

    __summary = None
    __images = None

    def __init__(self, url, summary, images):
        super().__init__(url)

        self.__summary = summary
        self.__images = images

    @classmethod
    def get_records_real_estate(cls):
        return cls.get_records(cls.__URL_REAL_ESTATE__)

    @classmethod
    def get_records_mortgage(cls):
        return cls.get_records(cls.__URL_MORTGAGE__)

    @classmethod
    def get_records_economic(cls):
        return cls.get_records(cls.__URL_ECONOMIC__)

    @classmethod
    def get_records(cls, url):
        records = OrderedDict()
        soup = PosterGrabber.get_html(url)
        for article in soup.select('article.article__grid'):
            article_url = cls.get_url(article)
            article_title = cls.get_title(article)

            article_summary = cls.build_summary([
                article_title,
                cls.get_summary(article),
            ])

            records[article_title] = RecordMortgageReports(
                article_url, article_summary,
                cls.get_images(article))

        return records

    @staticmethod
    def get_title(wrapper):
        return wrapper.select_one('h4.article__title').text

    @staticmethod
    def get_url(wrapper):
        return wrapper.select_one('h4.article__title a')['href']

    @staticmethod
    def get_summary(wrapper):
        return wrapper.select_one('div.article__excerpt').text

    @staticmethod
    def get_images(wrapper):
        style = wrapper.select_one('div.article__image')['style']
        background_url = style[style.find('background-image'):]
        background_url = background_url[background_url.find("'"):].strip("'")
        background_url = background_url[:background_url.find("'")].strip("'")

        return [background_url]

    def get_twitter_info(self):
        return self.__get_info()

    def get_facebook_info(self):
        return self.__get_info()

    def __get_info(self):
        return {
            'link':    self.url,
            'images':  self.__images,
            'text':    self.__summary,
        }
