from PosterRecord import *

import PosterGrabber
from collections import OrderedDict


class RecordRealtor (Record):
    __URL_LIST__ = 'https://www.realtor.com/advice/buy/'

    def __init__(self, url):
        super().__init__(url)

    @classmethod
    def get_records(cls):
        soup = PosterGrabber.get_html(
            url=cls.__URL_LIST__, encoding='UTF-8',
            method='GET', headers=cls.__get_request_headers())

        records = OrderedDict()
        for article in soup.select('div#primary article'):
            article_id = cls.get_id(article)
            if not article_id:
                break

            article_url = cls.get_url(article)
            records[article_id] = RecordRealtor(article_url)

        return records

    @staticmethod
    def __get_request_headers():
        return OrderedDict({
            ':authority': 'www.realtor.com',
            ':method': 'GET',
            ':path': '/advice/buy/',
            ':scheme': 'https',
        })

    @staticmethod
    def get_id(wrapper):
        article_id = wrapper['id']
        return None \
            if article_id.count('video') \
            else article_id

    @staticmethod
    def get_title(wrapper):
        tag = wrapper.select_one('header h1.headline')
        return tag.string.strip()

    @staticmethod
    def get_summary(wrapper):
        summary = str()
        for tag in wrapper.select('#article-body > p'):
            for string in tag.strings:
                summary += string.strip() + ' '

        return summary

    @staticmethod
    def get_url(wrapper):
        return wrapper.select_one('.entry-content a')['href']

    @staticmethod
    def get_images(wrapper):
        images = list()
        for tag in wrapper.select('figure img'):
            images.append(tag['src'])

        return images

    def get_twitter_info(self):
        return self.__get_info()

    def get_facebook_info(self):
        return self.__get_info()

    def __get_info(self):
        soup = PosterGrabber.get_html(self.url)
        article = soup.select_one('div#news-advice-article-page')

        images = self.get_images(article)
        text = RecordRealtor.build_summary([
            RecordRealtor.get_title(soup),
            RecordRealtor.get_summary(soup),
        ])

        return {
            'link':    self.url,
            'images':  images,
            'text':    text,
        }
