# custom
from PosterRecord import *

# system
from random import random, choice


class RecordKWRealty (Record):

    request_method = 'POST'
    request_scheme = 'https'
    request_base = 'homeoftampabay.kwrealty.com'
    request_search = '/ajax/listing/consumermapsearch/'

    tags_map = {
        'all': [
            '#HomeBuying', '#HouseHunting', '#FirstHome', '#HomeSale', '#HomesForSale', '#Property', '#JustListed',
            '#Properties', '#Investment', '#Home', '#Housing', '#Listing', '#Mortgage', '#EmptyNest'],
        'cheap': [
            '#FirstTimeHomeBuyer', '#DIY', '#DIYspecial', '#FixerUpper', '#Flipper', '#FlipHouse', '#TLCneeded',
            '#DesignerSpecial', '#FirstHome', '#PricedToGo', '#BargainHunt', '#Bargain', '#MostBangForYourBuck'],
        'medium': [
            '#GreatValue', '#RightPrice', '#GreatDeal', '#DoneRight', '#PerfectPrice', '#Renovated'],
        'expensive': [
            '#DreamHouse', '#LuxuryRealEstate', '#LuxuryLiving', '#MillionDollarListing']
    }

    def __init__(self, url):
        super().__init__(url)
        self.url = url

    @classmethod
    def get_records(cls):
        import PosterGrabber
        response_text = PosterGrabber.get_text(
            url=cls.request_base + cls.request_search,
            method=cls.request_method,
            headers=cls.__get_request_headers(),
            data=cls.__get_request_body(
                price=cls.__get_price_range()))

        import json
        response_json = json.loads(response_text)
        house_cards = response_json['payload']['listings']

        records = dict()
        for house in house_cards:
            house_id = house['id']
            house_url = house['detailsUrl']

            records[house_id] = \
                RecordKWRealty(
                    cls.request_scheme + '://' +
                    cls.request_base + house_url)

        return records

    @staticmethod
    def get_id(card):
        id_tag = card.select_one('span#mlsValue')
        return id_tag.string.strip()

    @staticmethod
    def get_images(card):
        tags = card.select('div.primary-carousel img.slider-image')

        images = list()
        for tag in tags:
            image_src = PosterHelper.crop_url(tag['data-lazy'])
            if not image_src.count('no_listing_image'):
                images.append('http:' + image_src)

        return images

    @staticmethod
    def get_summary(card):
        summary = str()
        summary_tag = card.select_one('div.summary')
        summary_text = summary_tag.strings
        for string in summary_text:
            string = string.strip()
            if string:
                summary += string + ' '

        return summary[:-1]

    @staticmethod
    def get_price(card):
        price_tag = card.select_one('span.price')
        return price_tag.string.strip()

    @classmethod
    def get_tags(cls, price: str):
        price = int(
            price
            .strip('$')
            .replace(',', str()))

        key_all = 'all'
        key_price = 'expensive' \
            if int(price) > 500000 else 'medium' \
            if int(price) > 200000 else 'cheap'

        return \
            '#RealEstate' + \
            ' ' + choice(cls.tags_map.get(key_all)) + \
            ' ' + choice(cls.tags_map.get(key_price))

    def __get_info(self):
        import PosterGrabber
        soup = PosterGrabber.get_html(super().url)
        card = soup.select_one('main.main-container div.row')

        self.id = self.get_id(card)
        self.price = self.get_price(card)
        self.summary = self.get_summary(card)

        self.images = self.get_images(card)
        if not len(self.images):
            raise PosterRecordException('Empty image set for this property')

        return {
            'link': self.url,
            'images': self.images,
        }

    @classmethod
    def __get_request_headers(cls):
        return {
            ':method': cls.request_method,
            ':scheme': cls.request_scheme,
            ':authority': cls.request_base,
            ':path': cls.request_search,
            'accept': '*/*',
            'x-requested-with': 'XMLHttpRequest',
            'accept-encoding': 'gzip, deflate, br',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        }

    @staticmethod
    def __get_request_body(price: int):
        price_max = int(price * 1.25)
        price_min = int(price * 0.75)

        return \
            '&lulat=28.3&lulong=-82.8' \
            '&rllat=27.8&rllong=-82.2' \
            '&favoritesonly=0' \
            '&maxprice={price_max}' \
            '&minprice={price_min}' \
            '&daysonwebsite=14' \
            '&propertytype[]=SINGLE' \
            '&propertytype[]=CONDO' \
            '&listingtype%5B%5D=Resale+New' \
            '&hasagerestrictions=2' \
            '&statusfilter=1' \
            '&stories=0'.format(
                price_min=price_min,
                price_max=price_max)
        # '&sort=mlsupdatedate+desc' \
        # '&isCommercial=' \
        # '&moreareas=' \
        # 'areas=' \
        # '&beds=' \
        # '&baths=' \
        # '&geopoints=' \
        # '&minsqft=' \
        # '&maxsqft=' \
        # '&minacres=' \
        # '&maxacres=' \
        # '&minyearbuilt=' \
        # '&maxyearbuilt=' \
        # '&subdivision=' \

    @staticmethod
    # custom function
    # to get value between 150K and 2M
    # value - float parameter from 0 to 10
    def __get_price_range(value=random() * 10):
        return (pow(value, 3.2) + 17 * value + 150) * 1000

    def get_twitter_info(self):
        info = self.__get_info()

        info['text'] = \
            self.get_tags(self.price) + ' ' + \
            self.price + ': ' + self.summary

        return info

    def get_facebook_info(self):
        info = self.__get_info()

        info['text'] = \
            self.get_tags(self.price) + ' ' + \
            'Price: ' + self.price + '! ' + \
            self.summary

        return info
