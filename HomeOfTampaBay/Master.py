# custom
from HomeOfTampaBay.RecordMortgageReports import *
from HomeOfTampaBay.RecordKWRealty import *
from HomeOfTampaBay.RecordRealtor import *

from PosterMaster import *
import PosterHelper


class Master (PosterMaster):

    def __init__(self):
        super().__init__()
        super().register_task('kwrealty', RecordKWRealty.get_records)
        super().register_task('realtor', RecordRealtor.get_records)
        super().register_task('reports', [
            RecordMortgageReports.get_records_real_estate,
            RecordMortgageReports.get_records_mortgage,
            RecordMortgageReports.get_records_economic,
        ])

    @staticmethod
    def get_twitter_token():
        return {
            'consumer_key': 'gDGUnKgf7Pj8YInqoeKT4PHDJ',
            'consumer_secret': 'LGjYlSYa5Z6beCnD5WXSY615fXRY9i7eOTp2H8FBUEbfuzIvVZ',
            'access_key': '1411815349-lQy2Om1LVcuRamjRlxLcGgT050CmcjAXJR2UPpX',
            'access_secret': 'CHyauYMUNkypp5S0GxwyehQMno99vt1eM4riKqI8j0v9x',
        }

    @staticmethod
    def get_facebook_token():
        return PosterHelper.get_facebook_token('202051909850508')
