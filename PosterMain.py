# system
import argparse

# custom
import PosterHelper


def load_masters(masters_path='.'):
    import os
    result = dict()
    for directory in os.listdir(masters_path):
        module_path = masters_path + os.sep + directory
        if True \
                and os.path.isdir(module_path) \
                and os.path.exists(module_path + os.sep + "__init__.py"):
            import_path = module_path.replace(os.sep, '.').lstrip('.')
            result[directory] = __import__(import_path)

    return result


def main():
    assert load_masters()
    masters = PosterHelper.masters()

    parser = argparse.ArgumentParser()
    parser.add_argument('--user',   type=str, choices=masters.keys())
    parser.add_argument('--media',  type=str, choices=['twitter', 'facebook'])

    bool_true = ['true', 'yes', '1']
    parser.add_argument('--debug', default=False,
                        type=lambda s: s.lower() in bool_true)

    args = argparse.Namespace()
    parser.parse_known_args(namespace=args)

    return action(args)


def action(args):
    assert args

    master_class = \
        PosterHelper.masters() \
        .get(args.user, None)
    assert master_class

    master = master_class()
    assert master

    publisher = {
        'twitter': {
            'method': PosterHelper.post_twitter_status,
            'token':  PosterHelper.debug_twitter_token
            if args.debug else master.get_twitter_token, },
        'facebook': {
            'method': PosterHelper.post_facebook_record,
            'token':  PosterHelper.debug_facebook_token
            if args.debug else master.get_facebook_token, }
    }.get(args.media, None)

    assert publisher

    records = master.get_records()
    assert len(records)

    publisher_method = publisher.get('method', None)
    assert publisher_method

    publisher_token = publisher.get('token', None)
    assert publisher_token

    for record_id in records.keys():
        record = records[record_id]
        print("Record '{rec_id}' publish started"
              .format(rec_id=record_id))

        from PosterRecord import PosterRecordException
        if not master.is_record_exists(record_id):

            if record_id is not record.url:
                print('\t' 'URL: {url}'.format(url=record.url))

            try:
                published = publisher_method(
                    record, publisher_token())
                assert published

                master.insert_record(record_id)
                break

            except PosterRecordException as error:
                print('\t' "logic 'Record' error occur: %s. Skipping it..." % error.message)

            except Exception as error:
                print('\t' "unknown error occur: %s" % error)

        else:
            print('\t' 'record was published earlier. Skipping it...')
            assert len(records)


if __name__ == "__main__":
    main()
